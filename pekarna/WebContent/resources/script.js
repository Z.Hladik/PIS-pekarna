var DrawEye = function(eyecontainer, eyepupil, speed, interval){
	var mouseX = 0, mouseY = 0, xp = 0, yp = 0;
	var limitX = $(eyecontainer).width() - $(eyepupil).width(),
		limitY = $(eyecontainer).height() - $(eyepupil).height(),
		offset = $(eyecontainer).offset();
	var asdf = $(eyecontainer);
	$(window).mousemove(function(e){
		mouseX = Math.min(e.pageX - offset.left, limitX);
		mouseY = Math.min(e.pageY - offset.top, limitY);
		if (mouseX < 0) mouseX = 0;
		if (mouseY < 0) mouseY = 0;
	});

	var follower = $(eyepupil);
	var loop = setInterval(function(){
		xp += (mouseX - xp) / speed;
		yp += (mouseY - yp) / speed;
		follower.css({left:xp, top:yp});
	}, interval);
};

window.onload = function() {
	var tmp = document.getElementById('home');
	if (tmp) document.getElementById("menu-home").className = 'active';
	tmp = document.getElementById('history');
	if (tmp) document.getElementById("menu-history").className = 'active';
	tmp = document.getElementById('doc');
	if (tmp) document.getElementById("menu-doc").className = 'active';
	tmp = document.getElementById('contact');
	if (tmp) document.getElementById("menu-contact").className = 'active';
	tmp = document.getElementById('login');
	if (tmp) document.getElementById("menu-login").className = 'active';
	
	// vyber data z kalendaru
	$.datepicker.setDefaults({
        closeText: 'Cerrar', 
        prevText: 'Předchozí', 
        nextText: 'Další', 
        currentText: 'Hoy', 
        monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
        monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'], 
        dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'], 
        dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',], 
        dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'], 
        weekHeader: 'Sm', 
		dateFormat: 'dd-mm-yy',
        firstDay: 1, 
        isRTL: false, 
        showMonthAfterYear: false, 
        yearSuffix: '',
        showAnim : "blind"
	});

	var id = document.getElementById("Form:calendar1");
	if (id) $(id).datepicker();
	id = document.getElementById("Form:calendar2");
	if (id) $(id).datepicker();
	id = document.getElementById("Form:calendar3");
	if (id) $(id).datepicker();



	var eye1 = new DrawEye("#left-eye",  "#left-pupil", 8, 30);
	var eye2 = new DrawEye("#right-eye", "#right-pupil", 8, 30);  
};

