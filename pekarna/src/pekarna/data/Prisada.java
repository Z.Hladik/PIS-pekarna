package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collection;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="prisada")
public class Prisada {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idPrisady;
	@Override
	public String toString() {
		return "Prisada [idPrisady=" + idPrisady + "]";
	}
	private String nazevPrisady;
	private double nakupniCena;
	private double mnozstviSkladem;
	private String jednotka;
	
	@OneToMany(fetch = EAGER, orphanRemoval = true, mappedBy = "prisada")
	private Collection<VyrobenZ> prodkuty;
	
	public int getIdPrisady() {
		return idPrisady;
	}
	public void setIdPrisady(int idPrisady) {
		this.idPrisady = idPrisady;
	}
	public String getNazevPrisady() {
		return nazevPrisady;
	}
	public void setNazevPrisady(String nazevPrisady) {
		this.nazevPrisady = nazevPrisady;
	}
	public double getNakupniCena() {
		return nakupniCena;
	}
	public void setNakupniCena(double nakupniCena) {
		this.nakupniCena = nakupniCena;
	}
	public double getMnozstviSkladem() {
		return mnozstviSkladem;
	}
	public void setMnozstviSkladem(double mnozstviSkladem) {
		this.mnozstviSkladem = mnozstviSkladem;
	}
	public String getJednotka() {
		return jednotka;
	}
	public void setJednotka(String jednotka) {
		this.jednotka = jednotka;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPrisady;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prisada other = (Prisada) obj;
		if (idPrisady != other.idPrisady)
			return false;
		return true;
	}
	public Collection<VyrobenZ> getProdkuty() {
		return prodkuty;
	}
	public void setProdkuty(Collection<VyrobenZ> prodkuty) {
		this.prodkuty = prodkuty;
	}
}
