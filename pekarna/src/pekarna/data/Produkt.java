package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.IO;

import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.PrintStream;
import java.util.Collection;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="produkt")
public class Produkt {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idProdukt;
	private String nazevProduktu;
	private double cena;
	private int skladem;
	@OneToMany(fetch = EAGER, orphanRemoval = true, mappedBy = "produkt")
	private Collection<SkladaSe> objednavky;
	@OneToMany(fetch = EAGER, mappedBy = "produkt")
	private Collection<VyrobenZ> prisady;
	
	public int getIdProdukt() {
		return idProdukt;
	}
	public void setIdProdukt(int idProdukt) {
		this.idProdukt = idProdukt;
	}
	public String getNazevProduktu() {
		return nazevProduktu;
	}
	public void setNazevProduktu(String nazevProduktu) {
		this.nazevProduktu = nazevProduktu;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public int getSkladem() {
		return skladem;
	}
	public void setSkladem(int skladem) {
		this.skladem = skladem;
	}
	public Collection<SkladaSe> getObjednavky() {
		return objednavky;
	}
	public void setObjednavky(Collection<SkladaSe> objednavky) {
		this.objednavky = objednavky;
	}
	public Collection<VyrobenZ> getPrisady() {
		return prisady;
	}
	public void setPrisady(Collection<VyrobenZ> prisady) {
		this.prisady = prisady;
	}
	@Override
	public String toString() {
		return "Produkt [idProdukt=" + idProdukt + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idProdukt;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produkt other = (Produkt) obj;
		if (idProdukt != other.idProdukt)
			return false;
		return true;
	}
}
