package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collection;

@Entity
@Table(name="adresa")
public class Adresa {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idAdresy;
	private String obec;
	private String ulice;
	private int cislo;
	private int psc;
	
	@ManyToOne(fetch = EAGER)
	private Zakaznik adresat;
	@ManyToOne(fetch = EAGER)
	private Oblast oblast;

	@OneToMany(fetch = EAGER, mappedBy="adresaDodani")
	private Collection<Objednavka> objednavky; 
	
	
	public Oblast getOblast() {
		return oblast;
	}
	public void setOblast(Oblast oblast) {
		this.oblast = oblast;
	}
	public int getidAdresy() {
		return idAdresy;
	}
	public void setidAdresy(int idAdresy) {
		this.idAdresy = idAdresy;
	}
	public String getObec() {
		return obec;
	}
	public void setObec(String obec) {
		this.obec = obec;
	}
	public int getCislo() {
		return cislo;
	}
	public void setCislo(int cislo) {
		this.cislo = cislo;
	}
	public String getUlice() {
		return ulice;
	}
	public void setUlice(String ulice) {
		this.ulice = ulice;
	}
	public int getPsc() {
		return psc;
	}
	public void setPsc(int psc) {
		this.psc = psc;
	}
	public Zakaznik getAdresat() {
		return adresat;
	}
	public void setAdresat(Zakaznik adresat) {
		this.adresat = adresat;
	}
	
	public void setObjednavky(Collection<Objednavka> objednavky) {
		this.objednavky = objednavky;
	}
	
	public Collection<Objednavka> getObjednavky() {
		return objednavky;
	}
	@Override
	public String toString() {
		return "Adresa [idAdresy=" + idAdresy + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAdresy;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adresa other = (Adresa) obj;
		if (idAdresy != other.idAdresy)
			return false;
		return true;
	}	
	
}
