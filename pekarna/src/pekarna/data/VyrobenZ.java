package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name="vyroben_z")
public class VyrobenZ {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idVyrobenZ;
	private double mnozstvi;
	private String jednotka;
	
	@ManyToOne(fetch = EAGER)
	private Produkt produkt;
	@ManyToOne(fetch = EAGER)
	private Prisada prisada;
	
	public int getIdVyrobenZ() {
		return idVyrobenZ;
	}
	public void setIdVyrobenZ(int idVyrobenZ) {
		this.idVyrobenZ = idVyrobenZ;
	}
	public double getMnozstvi() {
		return mnozstvi;
	}
	public void setMnozstvi(double mnozstvi) {
		this.mnozstvi = mnozstvi;
	}
	public String getJednotka() {
		return jednotka;
	}
	public void setJednotka(String jednotka) {
		this.jednotka = jednotka;
	}
	public Produkt getProdukt() {
		return produkt;
	}
	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	public Prisada getPrisada() {
		return prisada;
	}
	public void setPrisada(Prisada prisada) {
		this.prisada = prisada;
	}
}
