package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collection;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="zakaznik")
public class Zakaznik {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idZakaznika;
	private String jmeno;
	private String prijmeni;
	private String jmenoFirmy;
	
	@OneToMany(fetch = EAGER, mappedBy = "adresat", orphanRemoval = true, cascade = ALL)
	private Collection<Adresa> adresy;
	
	
	
	/*-----------------------------------------------------------------------------
	 * 	   GETTERY A SETTERY
	 -----------------------------------------------------------------------------*/
	public Collection<Adresa> getAdresy() {
		return adresy;
	}
	public void setAdresy(Collection<Adresa> adresy) {
		this.adresy = adresy;
	}
	public int getidZakaznika() {
		return idZakaznika;
	}
	public void setidZakaznika(int idZakaznika) {
		this.idZakaznika = idZakaznika;
	}
	public String getJmeno() {
		return jmeno;
	}
	public void setJmeno(String jmeno) {
		System.out.println(this.jmeno);
		this.jmeno = jmeno;
	}
	public String getPrijmeni() {
		return prijmeni;
	}
	public void setPrijmeni(String prijmeni) {
		System.out.println(this.prijmeni);
		this.prijmeni = prijmeni;
	}
	public String getjmenoFirmy() {
		return jmenoFirmy;
	}
	public void setjmenoFirmy(String jmenoFirmy) {
		this.jmenoFirmy = jmenoFirmy;
	}
	
}
