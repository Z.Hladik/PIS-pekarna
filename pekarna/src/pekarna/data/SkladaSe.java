package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name="sklada_se")
public class SkladaSe {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idSkladaSe;
	private short pocet;
	
	@ManyToOne(fetch = EAGER)
	private Objednavka objednavka;
	@ManyToOne(fetch = EAGER)
	private Produkt produkt;
	
	public int getIdSkladaSe() {
		return idSkladaSe;
	}
	public void setIdSkladaSe(int idSkladaSe) {
		this.idSkladaSe = idSkladaSe;
	}
	public short getPocet() {
		return pocet;
	}
	public void setPocet(short pocet) {
		this.pocet = pocet;
	}
	public Objednavka getObjednavka() {
		return objednavka;
	}
	public void setObjednavka(Objednavka objednavka) {
		this.objednavka = objednavka;
	}
	public Produkt getProdukt() {
		return produkt;
	}
	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
}
