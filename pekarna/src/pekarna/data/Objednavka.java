package pekarna.data;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="objednavky")
public class Objednavka {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idObjednavky;
	@Temporal(TemporalType.DATE)
	private Date datumObjednani;
	//private double cena;
	private String zpusobPlatby;
	private String zpusobDodani;
	@Temporal(TemporalType.DATE)
	private Date terminDodani;
	
	@ManyToOne(fetch = EAGER)
	private Adresa adresaDodani;	
	@ManyToOne(fetch = EAGER, cascade = ALL)
	private Zamestnanec dodal;
	@ManyToOne(fetch = EAGER, cascade = ALL)
	private Zamestnanec sestavoval;
	@OneToMany(fetch = EAGER, mappedBy = "objednavka")
	private Collection<SkladaSe> produkty;
	public Objednavka() {
		produkty = new ArrayList<SkladaSe>();
	}
	
	public int getIdObjednavky() {
		return idObjednavky;
	}
	public void setIdObjednavky(int idObjednavky) {
		this.idObjednavky = idObjednavky;
	}
	public Date getDatumObjednani() {
		return datumObjednani;
	}
	public void setDatumObjednani(Date datumObjednani) {
		this.datumObjednani = datumObjednani;
	}
	public String getZpusobPlatby() {
		return zpusobPlatby;
	}
	public void setZpusobPlatby(String zpusobPlatby) {
		this.zpusobPlatby = zpusobPlatby;
	}
	public String getZpusobDodani() {
		return zpusobDodani;
	}
	public void setZpusobDodani(String zpusobDodani) {
		this.zpusobDodani = zpusobDodani;
	}
	public Date getTerminDodani() {
		return terminDodani;
	}
	public void setTerminDodani(Date terminDodani) {
		this.terminDodani = terminDodani;
	}
	public Adresa getAdresaDodani() {
		return adresaDodani;
	}
	public void setAdresaDodani(Adresa adresaDodani) {
		this.adresaDodani = adresaDodani;
	}
	public Zamestnanec getSestavoval() {
		return sestavoval;
	}
	public void setSestavoval(Zamestnanec sestavoval) {
		this.sestavoval = sestavoval;
	}
	public Zamestnanec getDodal() {
		return dodal;
	}
	public void setDodal(Zamestnanec dodal) {
		this.dodal = dodal;
	}
	public Collection<SkladaSe> getProdukty() {
		return produkty;
	}
	public void setProdukty(Collection<SkladaSe> produkty) {
		this.produkty = produkty;
	}
}
