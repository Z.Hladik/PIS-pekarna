package pekarna.data;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name="oblast")
public class Oblast {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idOblast;
	private String nazev;
	private double taxaZaOdvoz;
	
	@OneToMany(fetch = EAGER, orphanRemoval = true, cascade = ALL, mappedBy = "oblast")
	private Collection<Adresa> adresy;
	@ManyToMany(fetch = EAGER, mappedBy = "dodavaciOblasti")
	private Collection<Zamestnanec> dodavatele;

	public Oblast(){
		adresy = new ArrayList<Adresa>();
		dodavatele = new ArrayList<Zamestnanec>();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idOblast;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Oblast other = (Oblast) obj;
		if (idOblast != other.idOblast)
			return false;
		return true;
	}

	public int getidOblast() {
		return idOblast;
	}
	public void setidOblast(int idOblast) {
		this.idOblast = idOblast;
	}
	public String getNazev() {
		return nazev;
	}
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}
	public double getTaxaZaOdvoz() {
		return taxaZaOdvoz;
	}
	public void setTaxaZaOdvoz (double taxaZaOdvoz) {
		this.taxaZaOdvoz = taxaZaOdvoz;
	}
	public Collection<Adresa> getAdresy() {
		return adresy;
	}
	public void setAdresy(Collection<Adresa> adresy) {
		this.adresy = adresy;
	}
	public Collection<Zamestnanec> getDodavatele() {
		return dodavatele;
	}
	public void setDodavatele(Collection<Zamestnanec> dodavatele) {
		this.dodavatele = dodavatele;
	}
}
