package pekarna.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.JoinTable;

@Entity
@Table(name="zamestnanec")
public class Zamestnanec {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int idZamestnanec;
	private String login;
	private String heslo;
	private String jmeno;
	private String prijemni;
	private String cisloUctu;
	@Temporal(TemporalType.DATE)
	private Date datumNastupu;
	@Temporal(TemporalType.DATE)
	private Date datumOdchodu;
	private String typZemstnance;
	
	@OneToMany(fetch = EAGER, orphanRemoval = true, mappedBy = "sestavoval")
	private Collection<Objednavka> sestaveneObjednavky;
	@OneToMany(fetch = EAGER, mappedBy = "dodal")
	private Collection<Objednavka> dodaneObjednavky;
	@ManyToMany(fetch = EAGER, cascade = ALL)
	@JoinTable(name = "dodava_do")
	private Collection<Oblast> dodavaciOblasti;

	public Zamestnanec(){
		dodavaciOblasti = new ArrayList<Oblast>();
		dodaneObjednavky = new ArrayList<Objednavka>();
		sestaveneObjednavky = new ArrayList<Objednavka>();
	}
	
	public int getidZamestnanec() {
		return idZamestnanec;
	}
	public void setidZamestnanec(int idZamestnanec) {
		this.idZamestnanec = idZamestnanec;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getHeslo() {
		return heslo;
	}
	public void setHeslo(String heslo) {
		this.heslo = heslo;
	}
	public String getJmeno() {
		return jmeno;
	}
	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}
	public String getPrijemni() {
		return prijemni;
	}
	public void setPrijemni(String prijemni) {
		this.prijemni = prijemni;
	}
	public String getCisloUctu() {
		return cisloUctu;
	}
	public void setCisloUctu(String cisloUctu) {
		this.cisloUctu = cisloUctu;
	}
	public Date getDatumNastupu() {
		return datumNastupu;
	}
	public void setDatumNastupu(Date datumNastupu) {
		this.datumNastupu = datumNastupu;
	}
	public Date getDatumOdchodu() {
		return datumOdchodu;
	}
	public void setDatumOdchodu(Date datumOdchodu) {
		this.datumOdchodu = datumOdchodu;
	}
	public String getTypZemstnance() {
		return typZemstnance;
	}
	public void setTypZemstnance(String typZemstnance) {
		this.typZemstnance = typZemstnance;
	}
	public Collection<Objednavka> getSestaveneObjednavky() {
		return sestaveneObjednavky;
	}
	public void setSestaveneObjednavky(Collection<Objednavka> sestaveneObjednavky) {
		this.sestaveneObjednavky = sestaveneObjednavky;
	}
	public Collection<Objednavka> getDodaneObjednavky() {
		return dodaneObjednavky;
	}
	public void setDodaneObjednavky(Collection<Objednavka> dodaneObjednavky) {
		this.dodaneObjednavky = dodaneObjednavky;
	}
	public Collection<Oblast> getDodavaciOblasti() {
		return dodavaciOblasti;
	}
	public void setDodavaciOblasti(Collection<Oblast> dodavaciOblasti) {
		this.dodavaciOblasti = dodavaciOblasti;
	}

	@Override
	public String toString() {
		return "Zamestnanec [idZamestnanec=" + idZamestnanec + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idZamestnanec;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zamestnanec other = (Zamestnanec) obj;
		if (idZamestnanec != other.idZamestnanec)
			return false;
		return true;
	}
	
}
