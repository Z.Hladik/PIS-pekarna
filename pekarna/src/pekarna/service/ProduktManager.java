package pekarna.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Prisada;
import pekarna.data.Produkt;

@Stateless
public class ProduktManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(Produkt p)
    {
    	em.merge(p);
    }
	
    public void remove(Produkt p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Produkt> findAll()
    {
    	return em.createQuery("SELECT o FROM Produkt o", Produkt.class).getResultList();
    }
    
    /*public List<Prisada> findPrisady(Produkt produkt)
    {
    	return em.createQuery("SELECT o FROM Produkt p "
    			+ "INNER JOIN VyrobenZ v ON v.produkt.idProdukt = p.idProdukt"
    			+ " INNER JOIN Prisada o ON v.prisada.idPrisady = o.idPrisady"
    			+ " WHERE p.idProdukt = :id ",Prisada.class).setParameter("id", produkt.getIdProdukt()).getResultList();	
	}*/
}
