package pekarna.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Oblast;

@Stateless
public class OblastManager {
	@PersistenceContext
	private EntityManager em;
	
	public Oblast save(Oblast p)
    {
    	return em.merge(p);
    }
	
    public void remove(Oblast p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Oblast> findAll()
    {
    	return em.createQuery("SELECT o FROM Oblast o", Oblast.class).getResultList();
    }

	public Oblast findById(int id) {
		Oblast ob = em.createQuery("SELECT o FROM Oblast o WHERE o.idOblast = :id", Oblast.class).setParameter("id", id).getResultList().get(0);
		return ob;
	}
}
