package pekarna.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Adresa;
import pekarna.data.Objednavka;
import pekarna.data.Zakaznik;

@Stateless
public class ZakaznikManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(Zakaznik p)
    {
    	em.merge(p);
    }
	
    public void remove(Zakaznik p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Zakaznik> findAll()
    {
    	return em.createQuery("SELECT o FROM Zakaznik o", Zakaznik.class).getResultList();
    }
    
    public List<Objednavka> findObjednavky(Zakaznik zakaznik) {
    	List<Objednavka> list = new ArrayList<Objednavka>();
		for (Adresa a : zakaznik.getAdresy())
		{
			for(Objednavka o : a.getObjednavky())
			{
				if(!list.contains(o))
					list.add(o);
			}
		}
		return list;
	}
}
