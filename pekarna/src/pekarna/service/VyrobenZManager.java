package pekarna.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.VyrobenZ;

@Stateless
public class VyrobenZManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(VyrobenZ p)
    {
    	em.merge(p);
    }
	
    public void remove(VyrobenZ p)
    {
    	em.remove(em.merge(p));
    }
}
