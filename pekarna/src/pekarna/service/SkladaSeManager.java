package pekarna.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.SkladaSe;

@Stateless
public class SkladaSeManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(SkladaSe p)
    {
    	em.merge(p);
    }
	
    public void remove(SkladaSe p)
    {
    	em.remove(em.merge(p));
    }
}
