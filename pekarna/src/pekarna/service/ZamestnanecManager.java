package pekarna.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Zamestnanec;

@Stateless
public class ZamestnanecManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(Zamestnanec p)
    {
    	em.merge(p);
    }
	
    public void remove(Zamestnanec p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Zamestnanec> findAll()
    {
    	return em.createQuery("SELECT o FROM Zamestnanec o", Zamestnanec.class).getResultList();
    }
    
    public List<Zamestnanec> findDrivers()
    {
    	return em.createQuery("SELECT o FROM Zamestnanec o WHERE o.typZemstnance LIKE 'driver'", Zamestnanec.class).getResultList();
    }
    
    public List<Zamestnanec> findSellers()
    {
    	return em.createQuery("SELECT o FROM Zamestnanec o WHERE o.typZemstnance LIKE 'seller'", Zamestnanec.class).getResultList();
    }
    
}
