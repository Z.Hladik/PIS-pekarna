package pekarna.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Adresa;

@Stateless
public class AdresaManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(Adresa p)
    {
    	em.merge(p);
    }
	
    public void remove(Adresa p)
    {
    	em.remove(em.merge(p));
    }
}
