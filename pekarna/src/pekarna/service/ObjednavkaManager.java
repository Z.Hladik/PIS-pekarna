package pekarna.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Objednavka;
import pekarna.data.SkladaSe;

@Stateless
public class ObjednavkaManager {
	@PersistenceContext
	private EntityManager em;
	
	public Objednavka save(Objednavka p)
    {
    	return em.merge(p);
    }
	
    public void remove(Objednavka p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Objednavka> findAll()
    {
    	return em.createQuery("SELECT o FROM Objednavka o", Objednavka.class).getResultList();
    }
    
    public double countPrize(Objednavka o) {
    	double d = 0.0;
    	for (SkladaSe se : o.getProdukty()) {
			d += se.getPocet()*se.getProdukt().getCena();
		}
    	
    	return d;
	}
}