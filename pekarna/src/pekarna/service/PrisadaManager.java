package pekarna.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pekarna.data.Prisada;

@Stateless
public class PrisadaManager {
	@PersistenceContext
	private EntityManager em;
	
	public void save(Prisada p)
    {
    	em.merge(p);
    }
	
    public void remove(Prisada p)
    {
    	em.remove(em.merge(p));
    }
    
    public List<Prisada> findAll()
    {
    	return em.createQuery("SELECT o FROM Prisada o", Prisada.class).getResultList();
    }
    
}
