package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;


import pekarna.data.Objednavka;
import pekarna.data.Oblast;
import pekarna.data.Zamestnanec;
import pekarna.service.ZamestnanecManager;

@ManagedBean(eager=true)
@SessionScoped
public class ZamestnanecBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ZamestnanecManager zamestnanecMgr;
	private Zamestnanec zamestnanec;
	private Oblast oblast;
	
	@ManagedProperty(value="#{oblastBean}")
	OblastBean oblastBn;
	
	public Boolean isRidic() {
		if (zamestnanec.getTypZemstnance() == null) {
			return false;
		}
		if (zamestnanec.getTypZemstnance().compareTo("driver") == 0) {
			return true;
		}
		return false;
	}
	
	public List<Objednavka> getObjednavkyZam() {
		if (isRidic()) {
			return (List<Objednavka>) zamestnanec.getDodaneObjednavky();
		}
		return (List<Objednavka>) zamestnanec.getSestaveneObjednavky();
	}
	
	public Zamestnanec getZamestnanec() {
		return zamestnanec;
	}

	public void setZamestnanec(Zamestnanec zamestnanec) {
		this.zamestnanec = zamestnanec;
	}

	public ZamestnanecManager getZamestnanecMgr() {
		return zamestnanecMgr;
	}

	public void setZamestnanecMgr(ZamestnanecManager zamestnanecMgr) {
		this.zamestnanecMgr = zamestnanecMgr;
	}

	public Oblast getOblast() {
		return oblast;
	}

	public void setOblast(Oblast oblast) {
		this.oblast = oblast;
	}

	public OblastBean getOblastBn() {
		return oblastBn;
	}

	public void setOblastBn(OblastBean oblastBn) {
		this.oblastBn = oblastBn;
	}

	public ZamestnanecBean() {
		zamestnanec = new Zamestnanec();
		oblast = new Oblast();
	}

	public List<Zamestnanec> getZamestnanci() {
		return zamestnanecMgr.findAll();
	}
	
	public List<Zamestnanec> getRidici() {
		return zamestnanecMgr.findDrivers();
	}
	
	public List<Zamestnanec> getProdavaci() {
		return zamestnanecMgr.findSellers();
	}
	
	public String checkZamestnanec(String login, String pass){
		List<Zamestnanec> list = this.getZamestnanci();
		Zamestnanec actual;
		for (int i = 0; i < list.size(); i++) {
			actual = list.get(i);
			if(login.equals(actual.getLogin()) && pass.equals(actual.getHeslo())){
				return actual.getTypZemstnance();
			}
		}
		return "";
	}
	//====================================================
    
	public String actionNew()
	{
		zamestnanec = new Zamestnanec();
		oblast = new Oblast();
		return "new";
	}
	
	public String actionEdit(Zamestnanec zamestnanec)
    {
    	setZamestnanec(zamestnanec);
    	return "edit";
    }
	
	public String actionUpdate()
    {
        zamestnanecMgr.save(zamestnanec);
        return "update";
    }
	
	public String actionInsertNew() {
		zamestnanecMgr.save(zamestnanec);
		return "insert";
	}
	    
    public String actionOblastAdd() {
    	if (isRidic()) {
			oblast.getDodavatele().add(zamestnanec);
			zamestnanec.getDodavaciOblasti().add(oblast);
		}
		return "addOblast";
	}
    
    public String actionOblastDel(Oblast obl) {
    	obl.getDodavatele().remove(zamestnanec);
    	zamestnanec.getDodavaciOblasti().remove(obl);
    	oblastBn.setOblast(obl);
    	oblastBn.actionUpdate();
		return "delOblast";
	}
    
    public String actionObjednavky(Zamestnanec zamestnanec) {
    	this.zamestnanec = zamestnanec;	
		return "objednavky";
	}
}
