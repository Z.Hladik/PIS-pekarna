package pekarna.back;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.*;
import javax.servlet.http.*;

public final class AuthenticationFilter implements Filter, Serializable
{
	private static final long serialVersionUID = 1L;
	private FilterConfig filterConfig = null;
    
/*    musi se implementovat, server ji vola, kdyz prijde pozadavek na url, ktere je v prirazeni
 * 	 rozhoduje, jestli je uzivatel prihlaseny
 */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
    {
        HttpSession session = ((HttpServletRequest) request).getSession();
        AuthenticationBean auth = (AuthenticationBean) session.getAttribute("authenticationBean");
        String type = filterConfig.getInitParameter("type");

        // uzivatel je prihlaseny
        if (auth != null && auth.isAuthorized(type))
        {
        	// pokracuje se ve zpracovani pozadavku (aplikuji se dalsi filtry, pokud jsou implementovany)
            chain.doFilter(request, response);
        }
        else
        {
        	// generuje presmerovani nekam (prihlaseni, ...)		zjisti korenove url + "cesta ke strance"
        	((HttpServletResponse) response).sendRedirect(((HttpServletRequest) request).getContextPath() + "/denied.xhtml");
            
        	// vygeneruji html, nema pristup
        /*	response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html><head><title>Bad login</title></head><body>");
            out.println("<h1>Access denied</h1>");
            out.println("Access denied. <a href=\"javascript:history.back()\">Go Back</a>.");
            out.println("</body></html>");*/
        }
    }

    public void destroy() 
    {
    }

    public void init(FilterConfig filterConfig) 
    {
        this.filterConfig = filterConfig;
    }
}
