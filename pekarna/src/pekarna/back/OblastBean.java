package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import pekarna.data.Adresa;
import pekarna.data.Oblast;
import pekarna.service.OblastManager;

@ManagedBean
@SessionScoped
public class OblastBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@EJB
	private OblastManager oblastMgr;
	private Oblast oblast;
	private Adresa adresa;
	
	public OblastBean() {
		oblast = new Oblast();
		adresa = new Adresa();
	}

	
	public Oblast getOblast() {
		return oblast;
	}
	public void setOblast(Oblast oblast) {
		this.oblast = oblast;
	}
	public List<Oblast> getOblasti() {
		return oblastMgr.findAll();
	}
	public Adresa getAdresa() {
		return adresa;
	}
	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}
	public Oblast getOblastById(int id) {
		return oblastMgr.findById(id);
	}
	//====================================================
    
	public String actionNew()
	{
		oblast = new Oblast();
		return "new";
	}
	public String actionInsertNew() {
		oblastMgr.save(oblast);
		return "insert";
	}
	public String actionUpdate()
    {
       setOblast(oblastMgr.save(oblast));
        return "update";
    }
    
    public String actionEdit(Oblast oblast)
    {
    	setOblast(oblast);
    	return "edit";
    }
    
    public String actionDelete(Oblast oblast)
    {
    	oblastMgr.remove(oblast);
    	return "delete";
    }
    
}
