package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import pekarna.data.Prisada;
import pekarna.data.Produkt;
import pekarna.data.VyrobenZ;
import pekarna.service.ProduktManager;
import pekarna.service.VyrobenZManager;

@ManagedBean
@SessionScoped
public class ProduktBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ProduktManager produktMgr;
	
	@EJB
	private VyrobenZManager vyrobenMgr;
	private Produkt produkt;
	private Prisada prisada;
	private VyrobenZ vyroben; 
	
	@ManagedProperty(value="#{prisadaBean}")
	private PrisadaBean prisBean;
	
	public PrisadaBean getPrisBean() {
		return prisBean;
	}

	public void setPrisBean(PrisadaBean prisBean) {
		this.prisBean = prisBean;
	}

	public VyrobenZ getVyroben() {
		return vyroben;
	}

	public void setVyroben(VyrobenZ vyroben) {
		this.vyroben = vyroben;
	}

	public ProduktBean() {
		produkt = new Produkt();
		prisada = new Prisada();
		vyroben = new VyrobenZ();
	}
	
		
	public Produkt getProdukt() {
		return produkt;
	}
	
	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	
	public List<Produkt> getProdukty() {
		return produktMgr.findAll();
		
	}
	
	public String actionEdit(Produkt produkt)
    {
    	setProdukt(produkt);
    	return "edit";
    }
	
	
	public String actionNew()
	{
		produkt = new Produkt();
		return "new";
	}
	
	public String actionInsertNew() {
		produktMgr.save(produkt);
		return "insert";
	}
	
	public String actionPrisadaNew()
    {
		vyroben = new VyrobenZ();
        prisada = new Prisada();
        return "newprisada";
    }
	
	public String actionPrisadaAdd() {
		vyroben.setProdukt(produkt);
		vyroben.setPrisada(prisada);
		produkt.getPrisady().add(vyroben);
		prisada.getProdkuty().add(vyroben);
		//prisBean.setPrisada(prisada);
		//prisBean.actionUpdate();
		return "add";
	}
	
	public String actionPrisadaDel(VyrobenZ vyr)
    {
        produkt.getPrisady().remove(vyr);
        vyr.getPrisada().getProdkuty().remove(vyr);
        prisBean.setPrisada(vyr.getPrisada());
        prisBean.actionUpdate();
        return "delete";
    }
	
	public Prisada getPrisada() {
		return prisada;
	}

	public void setPrisada(Prisada prisada) {
		this.prisada = prisada;
	}

	public String actionUpdate()
    {
        produktMgr.save(produkt);
        return "update";
    }
	

	public VyrobenZManager getVyrobenMgr() {
		return vyrobenMgr;
	}

	public void setVyrobenMgr(VyrobenZManager vyrobenMgr) {
		this.vyrobenMgr = vyrobenMgr;
	}

	
}
