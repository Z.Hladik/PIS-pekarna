package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.sun.xml.rpc.processor.modeler.j2ee.xml.emptyType;

import pekarna.data.Adresa;
import pekarna.data.Objednavka;
import pekarna.data.Zakaznik;
import pekarna.service.ZakaznikManager;

@ManagedBean
@SessionScoped
public class ZakaznikBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ZakaznikManager zakaznikMgr;
	private Zakaznik zakaznik;
	private Adresa adresa;
	
	@ManagedProperty(value="#{oblastBean}")
	private OblastBean oblastBn;
	
	public ZakaznikBean() {
		zakaznik = new Zakaznik();
		adresa = new Adresa();
	}

	
	public Zakaznik getZakaznik() {
		return zakaznik;
	}
	public void setZakaznik(Zakaznik zakaznik) {
		this.zakaznik = zakaznik;
	}
	public List<Zakaznik> getZakaznici() {
		return zakaznikMgr.findAll();
	}
	public Adresa getAdresa() {
		return adresa;
	}
	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}
	
	//====================================================
    
	public OblastBean getOblastBn() {
		return oblastBn;
	}


	public void setOblastBn(OblastBean oblastBn) {
		this.oblastBn = oblastBn;
	}
	
	public List<Objednavka> getObjednavky(){
		return zakaznikMgr.findObjednavky(zakaznik);
	}

	public String actionShowObj(Zakaznik zakaznik) {
		setZakaznik(zakaznik);
		return "showobj";
	}

	public String actionNew()
	{
		zakaznik = new Zakaznik();
		return "new";
	}
	public String actionInsertNew() {
		zakaznikMgr.save(zakaznik);
		return "insert";
	}
	public String actionUpdate()
    {
        zakaznikMgr.save(zakaznik);
        return "update";
    }
    
    public String actionEdit(Zakaznik zakaznik)
    {
    	setZakaznik(zakaznik);
    	return "edit";
    }
    
    public String actionDelete(Zakaznik zakaznik)
    {
    	zakaznikMgr.remove(zakaznik);
    	return "delete";
    }
    
    public String actionAdresaNew()
    {
        adresa = new Adresa();
        return "newadresa";
    }
    public String actionAdresaDel(Adresa adresa)
    {
        zakaznik.getAdresy().remove(adresa);
        oblastBn.setOblast(adresa.getOblast());
        oblastBn.getOblast().getAdresy().remove(adresa);
        oblastBn.actionUpdate();
        return "delete";
    }
    
    public String actionAdresaAdd()
    {
    	oblastBn.setOblast(adresa.getOblast());
        oblastBn.getOblast().getAdresy().add(adresa);
        oblastBn.actionUpdate();
        List <Adresa> list = (List<Adresa>) oblastBn.getOblast().getAdresy(); 
        Adresa adr = list.get(list.size() - 1);
        this.setAdresa(adr);
        
        adresa.setAdresat(zakaznik);
        zakaznik.getAdresy().add(adresa);
        
        return "add";
    }
    
    
}
