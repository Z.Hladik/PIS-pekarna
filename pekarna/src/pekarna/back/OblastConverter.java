package pekarna.back;

import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import pekarna.data.Oblast;
import pekarna.service.OblastManager;

@FacesConverter(value="oblastConverter")
public class OblastConverter implements Converter 
{
	@EJB
	private OblastManager oblastMgr;
	
	//ma dycky 2 metody pro konverzi obema smery
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent component, String value)
	{
		try {
			int id = Integer.parseInt(value);
			return oblastMgr.findById(id);
		} catch (NumberFormatException e) {
			throw new ConverterException("Invalid ID format");
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent component, Object value)
	{
		return Integer.toString(((Oblast) value).getidOblast());
	}

}
