package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import pekarna.data.Prisada;
import pekarna.service.PrisadaManager;


@ManagedBean
@SessionScoped
public class PrisadaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private PrisadaManager prisadaMgr;
	private Prisada prisada;
	public PrisadaBean() {
		prisada = new Prisada();
	}

	public Prisada getPrisada() {
		return prisada;
	}

	public void setPrisada(Prisada prisada) {
		this.prisada = prisada;
	}

	public List<Prisada> getPrisady() {
		return prisadaMgr.findAll();
	}
	
	public String actionNew()
	{
		prisada = new Prisada();
		return "new";
	}
    
	public String actionInsertNew() {
		prisadaMgr.save(prisada);
		return "insert";
	}
	
	public String actionEdit(Prisada prisada)
	{
	   	setPrisada(prisada);
	   	return "edit";
	}
	 
	public String actionUpdate()
	{
	    prisadaMgr.save(prisada);
	    return "update";
	}
}
