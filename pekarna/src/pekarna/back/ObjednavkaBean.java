package pekarna.back;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;


import pekarna.data.Objednavka;
import pekarna.data.Produkt;
import pekarna.data.SkladaSe;
import pekarna.data.Zakaznik;
import pekarna.service.ObjednavkaManager;

@ManagedBean
@SessionScoped
public class ObjednavkaBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private ObjednavkaManager objMgr;
	private Objednavka objednavka;
	private Zakaznik zakaznik;
	private Produkt produkt;
	private SkladaSe sklada;
	
	@ManagedProperty(value="#{zamestnanecBean}")
	private ZamestnanecBean zamBean;
	
	@ManagedProperty(value="#{zakaznikBean}")
	private ZakaznikBean zakBean;
	
	@ManagedProperty(value="#{produktBean}")
	private ProduktBean produktBean;
	
	
	public ObjednavkaBean() {
		objednavka = new Objednavka();
		zakaznik = new Zakaznik();
		produkt = new Produkt();
		sklada = new SkladaSe();
	}

	public Zakaznik getZakaznik() {
		return zakaznik;
	}

	public void setZakaznik(Zakaznik zakaznik) {
		this.zakaznik = zakaznik;
	}

	public Objednavka getObjednavka() {
		return objednavka;
	}

	public void setObjednavka(Objednavka objednavka) {
		this.objednavka = objednavka;
	}
	
	public List<Objednavka> getObjednavky(){
		return objMgr.findAll();
		
	}
	
	
	public ZamestnanecBean getZamBean() {
		return zamBean;
	}

	public void setZamBean(ZamestnanecBean zamBean) {
		this.zamBean = zamBean;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public ProduktBean getProduktBean() {
		return produktBean;
	}

	public void setProduktBean(ProduktBean produktBean) {
		this.produktBean = produktBean;
	}

	public SkladaSe getSklada() {
		return sklada;
	}

	public void setSklada(SkladaSe sklada) {
		this.sklada = sklada;
	}

	public double getPrice(Objednavka ofka){
		return objMgr.countPrize(ofka);
	}
	
	
	public ZakaznikBean getZakBean() {
		return zakBean;
	}

	public void setZakBean(ZakaznikBean zakBean) {
		this.zakBean = zakBean;
	}

	public String actionShow(Objednavka objednavka){
		setObjednavka(objednavka);
		return "show";
	}
	
	public String actionEdit(Objednavka objednavka) {
		setObjednavka(objednavka);
		zakaznik = objednavka.getAdresaDodani().getAdresat();
		return "edit";
	}
	
	public String actionNew(Zakaznik zakaznik) 
	{
		objednavka = new Objednavka();
		setZakaznik(zakaznik);
		return "addobjednavka";
	}
	
	public String actionInsertNew()
	{
		return "insert";
	}
	
	public String actionProduktAdd() {
		sklada.setObjednavka(objednavka);
		sklada.setProdukt(produkt);
		objednavka.getProdukty().add(sklada);
		produkt.getObjednavky().add(sklada);
			
		produkt = new Produkt();
		sklada = new SkladaSe();
		return "addprodukt";
	}
	
	public String actionSaveObj() {
		Objednavka obj = objMgr.save(objednavka);
		if(obj.getZpusobDodani().compareTo("vlastni") == 0)
		{
			obj.setDodal(null);
		}
		else {
			obj.getDodal().getDodaneObjednavky().add(obj);
			zamBean.setZamestnanec(obj.getDodal());
			zamBean.actionUpdate();
		}
		obj.getAdresaDodani().getObjednavky().add(obj);
		obj.getSestavoval().getSestaveneObjednavky().add(obj);
		zamBean.setZamestnanec(obj.getSestavoval());
		zamBean.actionUpdate();
		zakBean.setZakaznik(obj.getAdresaDodani().getAdresat());
		zakBean.actionUpdate();
		
		return "save";
	}
	
	//mela by byt na konec stejna jak saveObj
	public String actionUpdate() {
		objMgr.save(objednavka);
		return "save";
	}
	
	public String delProdukt(SkladaSe skl) {
		objednavka.getProdukty().remove(skl);
		skl.getProdukt().getObjednavky().remove(skl);
		produktBean.setProdukt(skl.getProdukt());
		produktBean.actionUpdate();
		
		return "delete";	
	}
	
	public String addProdukt() {
		return "addpr";
	}
	
	
	public boolean dodavano() {
		String dodani = getObjednavka().getZpusobDodani(); 
		if(dodani.compareTo("vlastni") == 0)
			return false;
		else
			return true;
	}
	
	public String returnToList(){
		return "return";
	}
	
	
}
