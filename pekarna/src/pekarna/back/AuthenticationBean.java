package pekarna.back;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class AuthenticationBean implements Serializable
{
	private static final long serialVersionUID = 1L;
	private boolean authorized;
    private String login;
    private String password;
    private String type = "";
    @ManagedProperty(value="#{zamestnanecBean}")
    private ZamestnanecBean bean;
    
    public ZamestnanecBean getBean() {
		return bean;
	}

	public void setBean(ZamestnanecBean bean) {
		this.bean = bean;
	}

	public AuthenticationBean()
    {
        authorized = false;
    }

    public boolean isAuthorized()
    {
        return authorized;
    }
    
    public boolean isAuthorized(String type)
    {
    	// uzivatel je odhlasen
    	if(this.type.equals(""))
    		return false;
    	switch (type) {
			case "ridic":
				return true;
			case "prodavac":
				return !this.type.equals("ridic");
			case "admin":
				return this.type.equals("admin");
			default:
				return false;
		}
    }
    public String isLogin(){
    	return (authorized)?"Odhlásit":"Přihlásit";
    }
    public Boolean canView(String type){
    	if(type.equals(this.type))
    		return true;
    	else if (type.equals("prodavac") && this.type.equals("admin"))
    		return true;
    	return false;
    }

    public void setAuthorized(boolean authorized)
    {
        this.authorized = authorized;
    }
    
    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String actionLogout()
    {
    	type = "";
        authorized = false;
        return "logout";
    }
    
    public String actionLogin()
    {
    	type = bean.checkZamestnanec(login, password);
    	switch (type) {
			case "admin":
				break;
			case "driver":
				type = "ridic";
				break;
			case "seller":
				type = "prodavac";
				break;
			default:
				type = "";
				authorized = false;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Neplatný login nebo heslo."));
	            return "failed";
        }
    	authorized = true;
		return "login";
    }
}